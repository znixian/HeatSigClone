/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone;

import heatsigclone.craft.CraftBase;
import heatsigclone.craft.PlayerCraft;
import heatsigclone.craft.SimpleAICraft;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Stack;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.KeyListener;
import xyz.znix.slickengine.MouseListener;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;
import xyz.znix.slickengine.objects.ObjectList;

/**
 *
 * @author znix
 */
public class IngameState implements State, MouseListener, KeyListener {

	/**
	 * How many milliseconds between craft updates. This is where we delete old
	 * crafts, and spawn new ones.
	 */
	private static final int UPDATE_CRAFTS_FREQUENCY = 1000 * 1;

	/**
	 * The distance inside of which a craft is counted as nearby, and will
	 * influence spawning.
	 */
	private static final int NEARBY_CRAFT_DIST = 250000;
	private static final long NEARBY_CRAFT_DIST_SQ = 1L * NEARBY_CRAFT_DIST * NEARBY_CRAFT_DIST;

	/**
	 * The farthest a craft eligible for despawning can go away from the player
	 * before it is destroyed.
	 */
	private static final long MAX_CRAFT_DIST_BEFORE_DESPAWN = NEARBY_CRAFT_DIST + 500;
	private static final long MAX_CRAFT_DIST_BEFORE_DESPAWN_SQ
			= MAX_CRAFT_DIST_BEFORE_DESPAWN * MAX_CRAFT_DIST_BEFORE_DESPAWN;

	/**
	 * The number of nearby crafts there can be, beyond which new crafts will
	 * stop spawning.
	 */
	private static final int MAX_NEARBY_CRAFT_FOR_SPAWNING = 250;

	private final ObjectList<CraftBase> objects;

	private final PlayerCraft player;
	private final Stack<CraftBase> focused;

	private float zoom = 1;

	private final boolean[] buttons = new boolean[9];
	private final boolean[] keys = new boolean[0xFF];
	private final Image backgroundBg;
	private final Image backgroundFg;
	private final Vector2f tmp;
	private final Random rand;

	private int mouseX, mouseY;
	private Context context;
	private int updateCraftTimer;

	public IngameState(SlickEngine engine, Resources resources) {
		objects = new ObjectList<>();
		focused = new Stack<>();
		objects.add(player = new PlayerCraft(this));
		focused.push(player);

		rand = new Random();

		tmp = new Vector2f();

		backgroundBg = resources.getImage("bg1-bg.png");
		backgroundFg = resources.getImage("bg1-fg.png");
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		objects.update(context, delta);

		updateCraftTimer += delta;
		if (updateCraftTimer > UPDATE_CRAFTS_FREQUENCY) {
			updateCraftTimer = 0;
			updateCrafts();
		}

		player.getPlayer().updateKeys(context, delta, keys);

		if (buttons[Input.MOUSE_LEFT_BUTTON]) {
			tmp.set(
					mouseX - context.getWidth() / 2,
					mouseY - context.getHeight() / 2
			);
			float scale = 150; // scaling of cursor distance
			float max = getFocused().getAccleration(); // max acceleration

			tmp.scale(1f / scale * max);

			float dist = tmp.length();
			dist = Math.min(dist, max);
			tmp.normalise().scale((float) dist);
			getFocused().accelerate(tmp, delta);
		} else if (buttons[Input.MOUSE_MIDDLE_BUTTON]) {
			objects.stream().filter(this::isCursorOver).findFirst().ifPresent((c) -> {
				getFocused().getVelocity().set(c.getVelocity());
			});
		} else if (buttons[Input.MOUSE_RIGHT_BUTTON]) {
			buttons[Input.MOUSE_RIGHT_BUTTON] = false;
			objects.stream().filter(this::isCursorOver).findFirst().ifPresent((c) -> {
				if (focused.contains(c)) {
					if (focused.size() == 1) {
						return;
					}
					CraftBase second = focused.get(focused.size() - 2);
					second.undockSelf();
					focused.pop();
				} else {
					CraftBase f = getFocused();
					if (c.canBeDocked(f)) {
						c.addDockedCraft(f);
						focused.push(c);
					}
				}
			});
		}
	}

	public boolean isCursorOver(CraftBase craft) {
		return craft.isInsideScreen(mouseX, mouseY);
	}

	@Override
	public void init(Context context) throws SlickException {
		State.super.init(context);
		this.context = context;
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setBackground(Color.black);
		g.clear();

		float umult = 0.25f;
		float zmult = 10f;

		drawBackground(g, backgroundFg, 0.05f * zmult, 0.025f * umult, 1, 0, 0);
//		drawBackground(g, backgroundBg, 0.05f, 0.025f, 1, 0, 0);

		drawBackground(g, backgroundBg, 0.075f * zmult, 0.05f * umult, 1.25f, 743, 549);
		drawBackground(g, backgroundBg, 0.10f * zmult, 0.075f * umult, 1.5f, 834, 123);

		drawBackground(g, backgroundBg, 0.15f * zmult, 0.1f * umult, 2, 0, 0);
//		drawBackground(g, backgroundFg, 0.15f, 0.1f, 2, 0, 0);

		drawBackground(g, backgroundBg, 0.20f * zmult, 0.125f * umult, 2.25f, 577, 534);

		focused.stream()
				.filter(c -> c instanceof SimpleAICraft)
				.map(c -> (SimpleAICraft) c)
				.forEach(c -> c.setDrawInterior(true));

		objects.render(context, g);

//		objects.stream().filter(this::isCursorOver).findFirst().ifPresent((c) -> {
//			g.setColor(Color.gray);
//			g.fillRect(c.getScreenX(), c.getScreenY(), c.getScreenWidth(), c.getScreenHeight());
//		});
	}

	private void drawBackground(Graphics g, Image background,
			float zoomMult,
			float speedMult,
			float scale,
			float offsetX,
			float offsetY) {
		CraftBase obj = getFocused();
		float bgZoom = getZoom() * zoomMult + 1; // zoom, rel to ship
		float totalScaling = scale * bgZoom;
		// fast to scale, but still cache
		Image scaled = background.getScaledCopy(totalScaling);

		float bgW = background.getWidth() * scale;
		float bgH = background.getHeight() * scale;
		float bgWS = bgW * bgZoom;
		float bgHS = bgH * bgZoom;
		float xOnBg = (obj.getX() + offsetX) / bgW;
		float yOnBg = (obj.getY() + offsetY) / bgH;

		float x = -xOnBg * speedMult * bgWS + context.getWidth() / 2;
		float y = -yOnBg * speedMult * bgHS + context.getHeight() / 2;

		x %= bgWS;
		y %= bgHS;
		x -= bgWS;
		y -= bgHS;

		for (; x < context.getWidth(); x += bgWS) {
			for (float ny = y; ny < context.getHeight(); ny += bgHS) {
				g.drawImage(scaled, x, ny);
			}
		}
	}

	public CraftBase getFocused() {
		return focused.peek();
	}

	public float getZoom() {
		return zoom;
	}

	public Context getContext() {
		return context;
	}

	@Override
	public void mouseWheelMoved(int change) {
		float diff = zoom / 2;
		if (change < 0) {
			diff = -diff;
		}
		zoom += diff;

		float max = 5f;
		float min = 0.005f;

		if (zoom > max) {
			zoom = max;
		}
		if (zoom < min) {
			zoom = min;
		}
	}

	@Override
	public void mouseMoved(int oldx, int oldy, int newx, int newy) {
		mouseX = newx;
		mouseY = newy;
	}

	@Override
	public void mouseDragged(int oldx, int oldy, int newx, int newy) {
		mouseX = newx;
		mouseY = newy;
	}

	@Override
	public void mousePressed(int button, int x, int y) {
		mouseX = x;
		mouseY = y;

		buttons[button] = true;
	}

	@Override
	public void mouseReleased(int button, int x, int y) {
		mouseX = x;
		mouseY = y;
		buttons[button] = false;
	}

	private void drawArrow(double dir, float dist, float spread) {
//		float x = (float) (context.getWidth() / 2 + dist * Math.cos(dir));
	}

	@Override
	public void keyPressed(int key, char c) {
		keys[key] = true;
	}

	@Override
	public void keyReleased(int key, char c) {
		keys[key] = false;
	}

	private void updateCrafts() {
		Vector2f base = getFocused().getPosition();

		List<CraftBase> toDespawn = new ArrayList<>();
		int nearbyCrafts = 0;

		for (CraftBase craft : objects) {
			float distSq = craft.getPosition().distanceSquared(base);
			if (distSq > MAX_CRAFT_DIST_BEFORE_DESPAWN_SQ) {
				if (craft.canDespawn()) {
					toDespawn.add(craft);
				}
			} else if (distSq <= NEARBY_CRAFT_DIST_SQ + 1000) {
				nearbyCrafts++;
			}
		}

		int numNewCraft = 0;

		if (nearbyCrafts < MAX_NEARBY_CRAFT_FOR_SPAWNING - 100) {
			numNewCraft = 75;
		} else if (nearbyCrafts < MAX_NEARBY_CRAFT_FOR_SPAWNING - 20) {
			numNewCraft = 25;
		} else if (nearbyCrafts < MAX_NEARBY_CRAFT_FOR_SPAWNING - 10) {
			numNewCraft = 5;
		} else if (nearbyCrafts < MAX_NEARBY_CRAFT_FOR_SPAWNING) {
			numNewCraft = 1;
		}

		for (int i = 0; i < numNewCraft; i++) {
			SimpleAICraft craft = new SimpleAICraft(this, rand);
			craft.getPosition().set(base);
			craft.getPosition().x += rand.nextInt(NEARBY_CRAFT_DIST) - NEARBY_CRAFT_DIST / 2;
			craft.getPosition().y += rand.nextInt(NEARBY_CRAFT_DIST) - NEARBY_CRAFT_DIST / 2;
			objects.add(craft);
		}

		toDespawn.stream().forEach(objects::remove);
	}
}

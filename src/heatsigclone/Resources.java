/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;

/**
 *
 * @author znix
 */
public class Resources {

	private int loaded;
	private final ArrayList<String> targets;
	private final Map<String, Object> resources;

	public Resources() {
		targets = new ArrayList<>();
		resources = new HashMap<>();

		targets.add("bg1-bg.png");
		targets.add("bg1-fg.png");
	}

	/**
	 * The tota number of assets to load
	 *
	 * @return
	 */
	public int getTargetCount() {
		return targets.size();
	}

	/**
	 * The number of assets loaded so far
	 *
	 * @return
	 */
	public int getLoaded() {
		return loaded;
	}

	/**
	 * Load the next resource. Does nothing if everything is loaded.
	 *
	 * @return True if everything has been loaded, false otherwise.
	 * @throws org.newdawn.slick.SlickException
	 */
	public boolean loadNext() throws SlickException {
		if (loaded == targets.size()) {
			return true;
		}

		String name = targets.get(loaded++);
		String extenstion = name.substring(name.lastIndexOf('.') + 1);
		switch (extenstion) {
			case "png":
				resources.put(name, new Image("resources/" + name));
				break;
			default:
				throw new UnsupportedOperationException("Unknown extension " + extenstion);
		}

		return false;
	}

	/**
	 * Get a image resource
	 *
	 * @param name The name of the image
	 * @return The image
	 */
	public Image getImage(String name) {
		return (Image) resources.get(name);
	}
}

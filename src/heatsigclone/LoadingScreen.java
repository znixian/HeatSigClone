/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.SlickEngine;
import xyz.znix.slickengine.State;

/**
 *
 * @author znix
 */
public class LoadingScreen implements State {

	private final Resources resources;
	private final SlickEngine engine;

	public LoadingScreen(SlickEngine engine) {
		this.engine = engine;
		this.resources = new Resources();
	}

	@Override
	public void init(Context context) throws SlickException {
		State.super.init(context);
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		boolean done = resources.loadNext();
		if (done) {
			engine.removeState();
			engine.addState(new IngameState(engine, resources));
		}
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setBackground(Color.black);
		g.clear();

		g.drawString(resources.getLoaded() + "/" + resources.getTargetCount(),
				context.getWidth() / 2, context.getHeight() / 2);
	}
}

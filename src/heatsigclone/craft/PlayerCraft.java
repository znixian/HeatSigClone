/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.craft;

import heatsigclone.IngameState;
import heatsigclone.interior.entity.EntityPlayer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import xyz.znix.slickengine.Context;

/**
 *
 * @author znix
 */
public class PlayerCraft extends CraftBase {

	private final EntityPlayer player;
	private final Vector2f acceleration;
	private final Vector2f tmp;

	public PlayerCraft(IngameState state) {
		super(state);
		player = new EntityPlayer();
		acceleration = new Vector2f();
		tmp = new Vector2f();
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		super.render(context, g);
		g.setColor(Color.yellow);
		g.fillRect(getScreenX(), getScreenY(), getScreenWidth(), getScreenHeight());

		tmp.set(acceleration);
		tmp.scale(1 / getAccleration());
		tmp.scale(150);
		g.setColor(Color.red);
		g.drawLine(getScreenX(), getScreenY(), getScreenX() + tmp.x, getScreenY() + tmp.y);

		tmp.set(velocity);
		tmp.normalise();
		tmp.scale(50);
		g.setColor(Color.cyan);
		g.drawLine(getScreenX(), getScreenY(), getScreenX() + tmp.x, getScreenY() + tmp.y);
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		super.update(context, delta);
		acceleration.set(0, 0);
	}

	@Override
	public void accelerate(Vector2f tmp, int delta) {
		super.accelerate(tmp, delta);
		acceleration.set(tmp);
	}

	@Override
	protected boolean canDockTo(CraftBase other) {
		if (!(other instanceof SimpleAICraft)) {
			return false;
		}
		int dist = 128;
		return getDistanceSquared(other.position) < dist * dist;
	}

	@Override
	protected void onDocked(CraftBase cb, Object data) {
		super.onDocked(cb, data);
	}

	public EntityPlayer getPlayer() {
		return player;
	}

	@Override
	public boolean canDespawn() {
		return false;
	}

	public float getAcceleration() {
		return 20f;
	}

	@Override
	public int getWidth() {
		return 64;
	}

	@Override
	public int getHeight() {
		return 64;
	}

}

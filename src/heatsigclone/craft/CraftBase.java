/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.craft;

import heatsigclone.IngameState;
import java.util.ArrayList;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.objects.WObject;

/**
 *
 * @author znix
 */
public abstract class CraftBase implements WObject {

	protected final Vector2f position; // casted in the get/set x/y methods
	protected final Vector2f velocity; // casted in the get/set x/y methods
	protected final IngameState state;
	protected double rotation;
	protected ArrayList<CraftBase> dockedCrafts;
	protected CraftBase dockedTo;
	protected Object dockingData;

	private Vector2f tmp;

	public CraftBase(IngameState state) {
		this.state = state;
		position = new Vector2f();
		velocity = new Vector2f();
		tmp = new Vector2f();
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		if (dockedCrafts != null) {
			for (CraftBase craft : dockedCrafts) {
				craft.render(context, g);
			}
		}
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		if (dockedTo != null) {
			dockedTo.updateDocked(this);
		} else {
			position.x += velocity.x;
			position.y += velocity.y;
		}

		if (dockedCrafts != null) {
			for (CraftBase craft : dockedCrafts) {
				craft.update(context, delta);
			}
		}
	}

	public void accelerate(Vector2f acc, int delta) {
		tmp.set(acc);
		tmp.scale(delta / 1000f);
		velocity.add(tmp);
	}

	@Override
	public int getX() {
		return (int) position.x;
	}

	@Override
	public int getY() {
		return (int) position.y;
	}

	public int getScreenX() {
//		if (dockedTo != null) {
//			return dockedTo.getScreenX() + 0;
//		}
		CraftBase focused = state.getFocused();
		float zoom = state.getZoom();

		return (int) ((position.x - focused.position.x - focused.getDisplayCenterX()) * zoom)
				+ state.getContext().getWidth() / 2;
	}

	public int getScreenY() {
//		if (dockedTo != null) {
//			return dockedTo.getScreenY() + 0;
//		}
		CraftBase focused = state.getFocused();
		float zoom = state.getZoom();

		return (int) ((position.y - focused.position.y - focused.getDisplayCenterY()) * zoom)
				+ state.getContext().getHeight() / 2;
	}

	public int getScreenWidth() {
		return (int) (getWidth() * state.getZoom());
	}

	public int getScreenHeight() {
		return (int) (getHeight() * state.getZoom());
	}

	public boolean isInsideScreen(int x, int y) {
		return WObject.isInside(x, y, // test x,y
				getScreenX(), getScreenY(), // our x,y
				getScreenWidth(), getScreenHeight() // our w,h
		);
	}

	public boolean canBeDocked(CraftBase craft) {
		return craft.canDockTo(this);
	}

	public void addDockedCraft(CraftBase craft) {
		addDockedCraft(craft, null);
	}

	protected void addDockedCraft(CraftBase craft, Object data) {
		if (dockedCrafts == null) {
			dockedCrafts = new ArrayList<>();
		}
		dockedCrafts.add(craft);
		craft.onDocked(this, data);
	}

	protected void onDocked(CraftBase craft, Object dockingData) {
		this.dockingData = dockingData;
		dockedTo = craft;
	}

	protected void removeDockedCraft(CraftBase craft) {
		if (dockedCrafts == null || craft.dockedTo != this) {
			return;
		}
		craft.dockedTo = null;
		dockedCrafts.remove(craft);
		if (dockedCrafts.isEmpty()) {
			dockedCrafts = null;
		}
	}

	public void undockSelf() {
		velocity.set(dockedTo.velocity);
		dockedTo.removeDockedCraft(this);
		dockedTo = null;
		dockingData = null;
	}

	public ArrayList<CraftBase> getDockedCrafts() {
		return dockedCrafts;
	}

	@Override
	public void setX(int x) {
		position.x = x;
	}

	@Override
	public void setY(int y) {
		position.y = y;
	}

	public double getRotation() {
		return rotation;
	}

	public void setRotation(double rotation) {
		this.rotation = rotation;
	}

	public Vector2f getPosition() {
		return position;
	}

	public Vector2f getVelocity() {
		return velocity;
	}

	protected boolean canDockTo(CraftBase other) {
		return false;
	}

	public float getDistanceSquared(Vector2f other) {
		float dx = other.getX() - getX();
		float dy = other.getY() - getY();
		return dx * dx + dy * dy;
	}

	public float getAccleration() {
		return 7.5f;
	}

	public boolean canDespawn() {
		return true;
	}

	protected float getDisplayCenterX() {
		return getWidth() / 2;
	}

	protected float getDisplayCenterY() {
		return getHeight() / 2;
	}

	protected void updateDocked(CraftBase other) {
		other.position.x = position.x;
		other.position.y = position.y;
		other.rotation = rotation;
	}

}

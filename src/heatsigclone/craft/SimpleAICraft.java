/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.craft;

import heatsigclone.IngameState;
import heatsigclone.gen.BuildingLikeGen;
import heatsigclone.gen.IRoom;
import heatsigclone.gen.RoomDocking;
import heatsigclone.interior.Cell;
import heatsigclone.interior.entity.EntityPlayer;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.function.Consumer;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.geom.Vector2f;
import xyz.znix.slickengine.Context;
import xyz.znix.slickengine.objects.WObject;

/**
 *
 * @author znix
 */
public class SimpleAICraft extends CraftBase {

	private static final int CELL_SIZE_ON_SCREEN = 32;

	protected final Cell[][] cells;
	protected final List<WObject> entities;
	protected final List<IRoom> rooms;

	private DrawMode drawMode = DrawMode.EXTERIOR;

	/**
	 * Has a player interacted with this craft. If so, do not deload it when the
	 * player gets too far away.
	 */
	private boolean interactedWith;

	public SimpleAICraft(IngameState state, Random rand) {
		super(state);
		entities = new ArrayList<>();
		BuildingLikeGen.GenData data = new BuildingLikeGen(rand).generate();
		cells = data.cells;
		rooms = data.rooms;
		velocity.set((float) (Math.random() * 10 - 5), (float) (Math.random() * 10 - 5));
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		int sx = getScreenX();
		int sy = getScreenY();

		switch (drawMode) {
			case INTERIOR: {
				g.pushTransform();
				g.translate(sx, sy);

				float cellScale = 1f * CELL_SIZE_ON_SCREEN / Cell.SIZE;
				float scale = state.getZoom() * cellScale;
				g.scale(scale, scale);

				// draw cells
				int x = 0;
				for (Cell[] col : cells) {
					int y = 0;
					for (Cell cell : col) {
						if (cell != null) {
							cell.render(context, g, x, y);
						}
						y++;
					}
					x++;
				}

				// draw entities
				for (WObject entity : entities) {
					entity.render(context, g);
				}

				g.popTransform();
				break;
			}
			case EXTERIOR: {
				float scaledCellSize = state.getZoom() * CELL_SIZE_ON_SCREEN;

				int x = 0;
				for (Cell[] col : cells) {
					int y = 0;
					for (Cell cell : col) {
						if (cell != null) {
							cell.drawExterior(context, g,
									sx + x * scaledCellSize,
									sy + y * scaledCellSize,
									scaledCellSize,
									scaledCellSize);
						}
						y++;
					}
					x++;
				}
				break;
			}
			case BLOCK: {
				float scaledCellSize = state.getZoom() * CELL_SIZE_ON_SCREEN;

				g.setColor(Color.lightGray);

				g.fillRect(
						sx,
						sy,
						scaledCellSize * cells.length,
						scaledCellSize * cells[0].length);
				break;
			}
		}

		updateDrawMode();

		super.render(context, g);
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
		super.update(context, delta);

		for (WObject entity : entities) {
			entity.update(context, delta);
		}
	}

	public void setDrawInterior(boolean drawInterior) {
		this.drawMode = drawInterior ? DrawMode.INTERIOR : DrawMode.EXTERIOR;
	}

	private void updateDrawMode() {
		int sx = getScreenX();
		int sy = getScreenY();

		boolean isVisible
				= sx + getScreenWidth() > 0
				&& sy + getScreenHeight() > 0
				&& sx < state.getContext().getWidth()
				&& sy < state.getContext().getHeight();

		if (!isVisible) {
			drawMode = DrawMode.CULLED;
		} else if (state.getZoom() < 0.025) { // zoomed out too far to see details
			drawMode = DrawMode.BLOCK;
		} else {
			drawMode = DrawMode.EXTERIOR;
		}
	}

	public Cell[][] getCells() {
		return cells;
	}

	public List<WObject> getEntities() {
		return entities;
	}

	@Override
	public int getWidth() {
		return cells.length * CELL_SIZE_ON_SCREEN;
	}

	@Override
	public int getHeight() {
		return cells[0].length * CELL_SIZE_ON_SCREEN;
	}

	@Override
	public void addDockedCraft(CraftBase craft) {
		DockingData data = new DockingData();
		data.room = getDockingPortFor(craft);
		data.x = craft.getX() - getX();
		data.y = craft.getY() - getY();
		super.addDockedCraft(craft, data);

		if (craft instanceof PlayerCraft) {
			PlayerCraft pc = (PlayerCraft) craft;
			EntityPlayer player
					= pc.getPlayer();
			interactedWith = true;

			player.reset(this);
			player.setX((data.room.getX() + 1) * Cell.SIZE);
			player.setY((data.room.getY() + 1) * Cell.SIZE);
			entities.add(player);
		}
	}

	@Override
	protected void removeDockedCraft(CraftBase craft) {
		super.removeDockedCraft(craft);
		if (craft instanceof PlayerCraft) {
			PlayerCraft pc = (PlayerCraft) craft;
			EntityPlayer player = pc.getPlayer();
			entities.remove(player);
			player.undock();
		}
	}

	@Override
	public boolean canBeDocked(CraftBase craft) {
		return getDockingPortFor(craft) != null;
	}

	private RoomDocking getDockingPortFor(CraftBase craft) {
		Optional<RoomDocking> port = rooms.stream()
				.filter(r -> (r instanceof RoomDocking))
				.map(r -> (RoomDocking) r).filter(r -> {
			int px = (int) craft.getX();
			int py = (int) craft.getY();
			px -= position.x;
			py -= position.y;

			px /= CELL_SIZE_ON_SCREEN;
			py /= CELL_SIZE_ON_SCREEN;

			return r.isInside(px, py);
		}).findAny();

		return port.orElse(null);
	}

	@Override
	protected void updateDocked(CraftBase other) {
		DockingData data = (DockingData) other.dockingData;
		other.position.x = position.x + data.x;
		other.position.y = position.y + data.y;
		other.rotation = rotation;
	}

	public boolean isInteractedWith() {
		return interactedWith;
	}

	@Override
	public boolean canDespawn() {
		return !isInteractedWith();
	}

	@Override
	protected float getDisplayCenterX() {
		Optional<WObject> player = entities.stream()
				.filter(e -> (e instanceof EntityPlayer))
				.findFirst();
		if (!player.isPresent()) {
			return super.getDisplayCenterX();
		}
		float cellScale = 1f * CELL_SIZE_ON_SCREEN / Cell.SIZE;
		return player.get().getX() * cellScale;
	}

	@Override
	protected float getDisplayCenterY() {
		Optional<WObject> player = entities.stream()
				.filter(e -> (e instanceof EntityPlayer))
				.findFirst();
		if (!player.isPresent()) {
			return super.getDisplayCenterY();
		}
		float cellScale = 1f * CELL_SIZE_ON_SCREEN / Cell.SIZE;
		return player.get().getY() * cellScale;
	}

	public enum DrawMode {
		BLOCK, EXTERIOR, INTERIOR, CULLED;
	}

	private class DockingData {

		private RoomDocking room;
		private float x, y;
	}

}

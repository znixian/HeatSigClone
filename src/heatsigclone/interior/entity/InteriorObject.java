/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.interior.entity;

import heatsigclone.craft.SimpleAICraft;
import heatsigclone.interior.Cell;
import xyz.znix.slickengine.objects.BasicObject;

/**
 * A basic object that is contained in a craft.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public abstract class InteriorObject extends BasicObject {

	protected SimpleAICraft craft;

	public InteriorObject(SimpleAICraft craft) {
		this.craft = craft;
	}

	public InteriorObject() {
	}

	/**
	 * Checks if a space is clear - if we could safely move to this point
	 * without hitting a wall.
	 *
	 * @param x The X part of the position
	 * @param y The Y part of the position
	 * @return {@code true} if the space is clear, {@code false} otherwise.
	 */
	public boolean spaceClear(int x, int y) {
		// find the cell with the lowest x,y values (start cell)
		int sx = x / Cell.SIZE;
		int sy = y / Cell.SIZE;

		// find the cell with the heighest x,y values (end cell)
		int ex = (x + getWidth()) / Cell.SIZE + 1;
		int ey = (y + getHeight()) / Cell.SIZE + 1;

		// cache the cells
		Cell[][] cells = craft.getCells();

		// for each cell
		for (int gx = sx; gx < ex; gx++) {
			for (int gy = sy; gy < ey; gy++) {
				// check that cell
				Cell cell = cells[gx][gy];
				if (cell == null || cell.isSolid()) {
					return false;
				}
			}
		}

		// we're clear
		return true;
	}

	/**
	 * Move by a given number of pixels in each direction, stopping if we
	 * collide with something.
	 *
	 * @param dx The number of pixels to move in the X direction
	 * @param dy The number of pixels to move in the Y direction
	 */
	protected void moveInDir(int dx, int dy) {
		// the value (absolute) x and y
		int vx = Math.abs(dx);
		int vy = Math.abs(dy);

		// the sign of the x and y
		int sx = (int) Math.signum(dx);
		int sy = (int) Math.signum(dy);

		// move in the x direction
		for (int i = 0; i < vx; i++) {
			int nx = x + sx;
			if (!spaceClear(nx, y)) {
				return;
			}
			x = nx;
		}

		// move in the y direction
		for (int i = 0; i < vy; i++) {
			int ny = y + sy;
			if (!spaceClear(x, ny)) {
				return;
			}
			y = ny;
		}
	}
}

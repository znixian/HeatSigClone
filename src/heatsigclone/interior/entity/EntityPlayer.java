/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.interior.entity;

import heatsigclone.craft.SimpleAICraft;
import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Input;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class EntityPlayer extends InteriorObject {

	/**
	 * Reset the player before entering a new craft.
	 */
	public void reset(SimpleAICraft craft) {
		this.craft = craft;

		// reset things
		spaceClear(0, 0);
		spaceClear(20, 20);
		spaceClear(30, 30);
	}

	@Override
	public int getWidth() {
		return 80;
	}

	@Override
	public int getHeight() {
		return 80;
	}

	@Override
	public void update(Context context, int delta) throws SlickException {
	}

	public void updateKeys(Context context, int delta, boolean[] keys) throws SlickException {
		if (craft == null) {
			return;
		}

		int speed = 1;
		if (keys[Input.KEY_W]) {
			moveInDir(0, -speed);
		}
		if (keys[Input.KEY_S]) {
			moveInDir(0, speed);
		}
		if (keys[Input.KEY_D]) {
			moveInDir(speed, 0);
		}
		if (keys[Input.KEY_A]) {
			moveInDir(-speed, 0);
		}
	}

	@Override
	public void render(Context context, Graphics g) throws SlickException {
		g.setColor(Color.orange);
		g.fillRect(getX(), getY(), getWidth(), getHeight());
	}

	public void undock() {
		craft = null;
	}

}

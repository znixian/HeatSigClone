/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.interior;

import org.newdawn.slick.Color;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.SlickException;
import xyz.znix.slickengine.Context;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class EmptyCell implements Cell {

	@Override
	public void update(Context context, int delta) throws SlickException {
	}

	@Override
	public void render(Context context, Graphics g, int x, int y) throws SlickException {
		g.setColor(Color.blue);
		g.fillRect(x * 100, y * 100, 100, 100);
	}

	@Override
	public boolean isSolid() {
		return false;
	}

	@Override
	public int getPathWeight() {
		return 20;
	}

}

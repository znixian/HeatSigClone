/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import java.util.List;
import java.util.Map;
import xyz.znix.slickengine.objects.WObject;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public abstract class SimpleRoom implements IRoom {

	protected int minWidth, minHeight;
	protected int x, y, width, height;

	public SimpleRoom(int width, int height) {
		this.minWidth = width;
		this.minHeight = height;
	}

	public SimpleRoom() {
	}

	@Override
	public int getMinWidth() {
		return minWidth;
	}

	@Override
	public int getMinHeight() {
		return minHeight;
	}

	@Override
	public void apply(Map<Vector2i, Cell> cells, List<IRoom> childList,
			int x, int y, int width, int height) {
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}

	@Override
	public void offset(Vector2i offset) {
		x += offset.getX();
		y += offset.getY();
	}

	public boolean isInside(int px, int py) {
		return WObject.isInside(px, py, x, y, width, height);
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public int getWidth() {
		return width;
	}

	public int getHeight() {
		return height;
	}

}

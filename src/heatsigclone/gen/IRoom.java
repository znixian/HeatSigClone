/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public interface IRoom {

	int getMinWidth();

	int getMinHeight();

	void apply(Map<Vector2i, Cell> cells, List<IRoom> childList, int x, int y, int width, int height);

	void offset(Vector2i offset);
}

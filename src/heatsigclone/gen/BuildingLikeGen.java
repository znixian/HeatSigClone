/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Simple, split floorplan generator.
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class BuildingLikeGen {

	private final Random rand;
	private boolean madeDock;

	public BuildingLikeGen(Random rand) {
		this.rand = rand;
	}

	public GenData generate() {
		// reset everything
		madeDock = false;

		// Cells on this ship
		Map<Vector2i, Cell> cells = new HashMap<>();

		// the main room - in this case a container, a hallway
		IRoom main = new RoomHallway(rand, (space) -> {
			boolean outside = space.x == 0 || space.y == 0; // TODO complete
			if (outside && !madeDock) {
				madeDock = true;
				return new RoomDocking(rand);
			}
			switch (rand.nextInt(1)) {
				case 0:
					return new RoomStorage(rand);
				case 1: // TODO make second room
					return new RoomStorage(rand);
				default:
					throw new RuntimeException();
			}
		});

		List<IRoom> childList = new ArrayList<>();

		// add the room to the map
		main.apply(cells, childList, 0, 0, 15 + rand.nextInt(25), 15 + rand.nextInt(25));

		// if there are no cells, throw.
		if (cells.isEmpty()) {
			throw new IllegalStateException("Cannot have no cells!");
		}

		// find the size of the ship
		int maxx = 0;
		int maxy = 0;
		int minx = Integer.MAX_VALUE;
		int miny = Integer.MAX_VALUE;
		for (Vector2i pos : cells.keySet()) {
			maxx = Math.max(maxx, pos.getX() + 1);
			maxy = Math.max(maxy, pos.getY() + 1);

			minx = Math.min(minx, pos.getX());
			miny = Math.min(miny, pos.getY());
		}

		// an array of cells, as used elsewhere
		Cell[][] cellArray = new Cell[maxx - minx][maxy - miny];

		Vector2i offset = new Vector2i(-minx, -miny);

		childList.stream().forEach(r -> r.offset(offset));

		// add all the cells to the array
		cells.entrySet().stream().forEach((cell) -> {
			Vector2i pos = cell.getKey();
			cellArray[pos.getX() + offset.getX()][pos.getY() + offset.getY()] = cell.getValue();
		});

		return new GenData(cellArray, childList);
	}

	public static class GenData {

		public final Cell[][] cells;
		public final List<IRoom> rooms;

		private GenData(Cell[][] cells, List<IRoom> rooms) {
			this.cells = cells;
			this.rooms = rooms;
		}
	}
}

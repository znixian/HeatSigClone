/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import heatsigclone.interior.HallCell;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import org.newdawn.slick.Color;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class RoomHallway extends SimpleRoom {

	private static final int ROOM_GAP = 1;
	private static final int ROOM_BORDER_OFFSET = 1;

	private final Random rand;
	private final RoomProvider roomProvider;

	public RoomHallway(Random rand, RoomProvider roomProvider) {
		this.rand = rand;
		this.roomProvider = roomProvider;
	}

	@Override
	public void apply(Map<Vector2i, Cell> cells, List<IRoom> childList,
			int ox, int oy, int width, int height) {
		List<RoomSpace> spaces = new ArrayList<>();
		spaces.addAll(makeSpaces(ox, oy, width, height));

		Collections.shuffle(spaces, rand);
		spaces.stream().forEach((space) -> {
			IRoom room = roomProvider.generate(space);
			room.apply(cells, childList, space.x, space.y, space.width, space.height);
			childList.add(room);
		});

		for (RoomSpace space : spaces) {
			ArrayList<Vector2i> doorSpaces = space.getSurrounding(Phase.B, ROOM_BORDER_OFFSET);
			for (RoomSpace neighbour : space.neighbours) {
				ArrayList<Vector2i> toAdd = neighbour.getSurrounding(Phase.A, ROOM_BORDER_OFFSET);
				toAdd.retainAll(doorSpaces);
				Color c = new Color((float) Math.random(), (float) Math.random(), (float) Math.random());
				if (toAdd.isEmpty()) {
					continue; // on the wrong side.
				}
				int id = rand.nextInt(toAdd.size());
				cells.put(toAdd.get(id), new HallCell());
			}
		}

		EnclosedArea.enclose(cells);
	}

	private Set<RoomSpace> makeSpaces(int ox, int oy, int width, int height) {
		// Use sets to prevent duplicates.
		Set<RoomSpace> spaces = new HashSet<>();
		Set<RoomSpace> splittableSpaces = new HashSet<>();
		Set<RoomSpace> tmp = new HashSet<>();

		// make a root space
		RoomSpace root = new RoomSpace(ox, oy, width, height, rand);
		splittableSpaces.add(root);
		spaces.add(root);

		// make all the spaces
		while (true) {
			// keep going until all the rooms are too small to divide further
			if (splittableSpaces.isEmpty()) {
				break;
			}

			// find a random room
			int target = rand.nextInt(splittableSpaces.size());
			int i = 0;
			RoomSpace room = null;
			for (RoomSpace test : splittableSpaces) {
				if (target == i) {
					room = test;
				}
				i++;
			}
			if (room == null) {
				throw new NullPointerException();
			}

			// clear the tmp table
			tmp.clear();

			// split that room
			room.split(tmp, rand);

			// add the new rooms to spaces
			spaces.addAll(tmp);

			// add the new splittable rooms to splittableSpaces
			tmp.stream()
					.filter(s -> s.canSplit())
					.forEach(splittableSpaces::add);

			// remove the old room, as it is now split
			spaces.remove(room);
			splittableSpaces.remove(room);

			// remove it from neighbours, too
			spaces.stream().forEach(s -> s.neighbours.retainAll(spaces));
		}

		return spaces;
	}

	public static class RoomSpace {

		private static final int MIN_SIZE = 3;

		public final int x, y, width, height;
		private final boolean horizontal;
		private final int split;
		private List<RoomSpace> children;
		private List<RoomSpace> neighbours;

		public RoomSpace(int x, int y, int width, int height, Random rand) {
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;

			neighbours = new ArrayList<>();

			horizontal = rand.nextBoolean(); //direction of split
			int len = (horizontal ? width : height);
			int max = len - MIN_SIZE; //maximum height/width we can split off
			if (max <= MIN_SIZE) {
				// can't split
				split = -1;
			} else {
				split = len / 2;
//				split = rand.nextInt(max - MIN_SIZE) + MIN_SIZE; // generate split point
			}
		}

		public boolean canSplit() {
			return split != -1;
		}

		public void split(Set<RoomSpace> spaces, Random rand) {
			if (children != null) {
				throw new IllegalStateException();
			}

			children = new ArrayList<>();

			int ps = split;
			int gap = ROOM_GAP;

			if (horizontal) { //populate child areas
				children.add(new RoomSpace(x, y, ps, height, rand));
				children.add(new RoomSpace(x + ps + gap, y, width - ps - gap, height, rand));
			} else {
				children.add(new RoomSpace(x, y, width, ps, rand));
				children.add(new RoomSpace(x, y + ps + gap, width, height - ps - gap, rand));
			}
			spaces.addAll(children);

			children.stream().forEach((child) -> {
				child.neighbours.addAll(children);
				child.neighbours.remove(child);

				neighbours.stream().forEach((neighbour) -> {
					ArrayList<Vector2i> matching = child.getSurrounding(Phase.ALL, ROOM_BORDER_OFFSET);
					matching.retainAll(neighbour.getSurrounding(Phase.ALL, ROOM_BORDER_OFFSET));
					if (!matching.isEmpty()) {
						child.neighbours.add(neighbour);
						neighbour.neighbours.add(child);
					}
				});
			});
		}

		public ArrayList<Vector2i> getSurrounding() {
			return getSurrounding(Phase.ALL, 1);
		}

		public ArrayList<Vector2i> getSurrounding(Phase phase, int dist) {
			ArrayList<Vector2i> points = new ArrayList<>();

			for (int px = x; px < x + width; px++) {
				if (phase.a) {
					points.add(new Vector2i(px, y - dist));
				}
				if (phase.b) {
					points.add(new Vector2i(px, y + height - 1 + dist));
				}
			}
			for (int py = y; py < y + height; py++) {
				if (phase.a) {
					points.add(new Vector2i(x - dist, py));
				}
				if (phase.b) {
					points.add(new Vector2i(x + width - 1 + dist, py));
				}
			}

			return points;
		}

		@Override
		public String toString() {
			return "RoomSpace{" + "x=" + x + ", y=" + y + ", width=" + width + ", height=" + height + '}';
		}

	}

	public enum Phase {
		ALL(true, true), A(true, false), B(false, true);

		public final boolean a, b;

		private Phase(boolean a, boolean b) {
			this.a = a;
			this.b = b;
		}
	}

	public interface RoomProvider {

		IRoom generate(RoomSpace space);
	}
}

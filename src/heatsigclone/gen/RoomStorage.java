/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import heatsigclone.interior.EmptyCell;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class RoomStorage extends SimpleRoom {

	public RoomStorage(Random rand) {
		super(rand.nextInt(8) + 3, rand.nextInt(8) + 3);
	}

	@Override
	public void apply(Map<Vector2i, Cell> cells, List<IRoom> childList,
			int ox, int oy, int width, int height) {
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				Vector2i pos = new Vector2i(x + ox, y + oy);
				if (cells.containsKey(pos)) {
					throw new RuntimeException();
				}
				cells.put(pos, new EmptyCell());
			}
		}

		super.apply(cells, childList, ox, oy, width, height);
	}

}

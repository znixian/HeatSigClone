/* 
 * Copyright (C) 2016 Campbell Suter <znix@znix.xyz>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package heatsigclone.gen;

import heatsigclone.Vector2i;
import heatsigclone.interior.Cell;
import heatsigclone.interior.WallCell;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author Campbell Suter <znix@znix.xyz>
 */
public class EnclosedArea {

	private EnclosedArea() {
	}

	public static void enclose(Map<Vector2i, Cell> cells) {
		Set<Vector2i> outer = getSurrounding(cells);
		outer.stream().forEach(p -> cells.put(p, new WallCell()));
	}

	public static Set<Vector2i> getSurrounding(Map<Vector2i, Cell> cells) {
		Set<Vector2i> outer = new HashSet<>();
		cells.entrySet().stream()
				.filter(p -> !p.getValue().isExterior())
				.map(e -> e.getKey()).forEach((p) -> {
			int x = p.getX();
			int y = p.getY();
			testOuter(outer, cells, x + 1, y);
			testOuter(outer, cells, x - 1, y);
			testOuter(outer, cells, x, y + 1);
			testOuter(outer, cells, x, y - 1);

			testOuter(outer, cells, x + 1, y + 1);
			testOuter(outer, cells, x + 1, y - 1);
			testOuter(outer, cells, x - 1, y + 1);
			testOuter(outer, cells, x - 1, y - 1);
		});

		return outer;
	}

	public static void testOuter(Set<Vector2i> outer, Map<Vector2i, Cell> cells, int x, int y) {
		Vector2i vec = new Vector2i(x, y);
		if (!cells.containsKey(vec)) {
			outer.add(vec);
		}
	}
}
